package com.company;


public class API_Request {

    // adresa API-ului
    private String apiAddress;

    // cheia primită pe email
    private String apiKey;

    // link-ul finale către care vom face cererea. Contine parametrii GET necesari pentru cererea noastră.
    private String requestString;

    // Pagina returnata in urma cautarii unui film/serial dupa un cuvant cheie
    private char PAGE = '1';

    public API_Request() {
        this.apiAddress = "http://www.omdbapi.com";
        this.apiKey = "apikey=faff28d0";
       // this.requestString = apiAddress + "/?" +  "=" + title + "&" + apiKey;
    }

    public void buildRequestString(char searchKey, String whatToSearch){
        if(searchKey == 's') {
            this.requestString = apiAddress + "/?" + searchKey + "=" + whatToSearch + "&" + "page=" + PAGE  + "&" + apiKey;
        } else {
            this.requestString = apiAddress + "/?" + searchKey + "=" + whatToSearch + "&" + apiKey;
        }
    }

    public String getApiAddress() {
        return apiAddress;
    }

    public void setApiAddress(String apiAddress) {
        this.apiAddress = apiAddress;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getRequestString() {
        return requestString;
    }
}

