package com.company;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        int ok = 1;
        Interface myInterface = new Interface();
        while(ok != 0) {
            myInterface.showMainPage();
            myInterface.chooseOption();
        }
    }
}
