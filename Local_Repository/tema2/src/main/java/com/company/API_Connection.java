package com.company;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class API_Connection {

    private URL url;
    private HttpURLConnection con;
    private int status;
    private BufferedReader requestResult;

    public void openConnection(String requestString, String method) throws IOException {

        // creăm un obiect URL pe baza request-string-ului
        this.url = new URL(requestString);

        // Deschidem conexiunea
        con = (HttpURLConnection) url.openConnection();

        // setăm metoda HTTP
        this.con.setRequestMethod(method);

        // trimitem cererea si asteptam raspunsul. status va conține codul intors de cererea noastră(ex. 200).
        this.status = con.getResponseCode();

        // Creăm un BufferedReader pentru a citi informația de la linkul respectiv
        this.requestResult = new BufferedReader(new InputStreamReader(con.getInputStream()));
    }

    public void closeConnection() throws IOException {
        // Inchidem reader-ul si conexiunea
        this.requestResult.close();
        con.disconnect();
    }

    public String createJsonContent() throws IOException {

        // Vom salva informatia in format JSON intr-un StringBuilder
        StringBuilder content = new StringBuilder();

        // Citim toate liniile si le appendu-im in content
        String inputLine;
        while ((inputLine = this.requestResult.readLine()) != null) {
            content.append(inputLine);
        }

        String jsonContent = content.toString();
        closeConnection();
        return jsonContent;

    }
}
