package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Interface {
    private String mainPage;
    private Scanner input;
    private File seenMovies;
    private File toSeeMovies;
    private API_Connection apiConnection;
    private API_Request apiRequest;
    private JsonParser jsonParser;

    public Interface() throws IOException {
        // Construieste pagina principala
        this.mainPage = "Alegeti o optiune:\n" +
                "1. Afisare filme vizionate\n" +
                "2. Afisare filme de vizionat\n" +
                "3. Cautare film dupa titlu\n" +
                "4. Cautare film dupa id IMDB\n" +
                "5. Cautare film dupa cuvant cheie\n" +
                "6. Adauga film in lista de vizionate\n" +
                "7. Adauga film in lista de viitoare vizionari\n" +
                "8. Afisare rezumat film\n";

        // Scanner pentru a citi de la tastatura
        this.input = new Scanner( System.in );

        // Fisierul care contine filmele vizionate
        this.seenMovies = new File("src/main/resources/seenMovies.txt");

        // Fisierul care contine filmele de vizionat
        this.toSeeMovies = new File("src/main/resources/toSeeMovies.txt");

        // Obiectele necesare pentru conectare la OMDB API si pentru parsarea unui obiect json
        this.apiConnection = new API_Connection();
        this.apiRequest = new API_Request();
        this.jsonParser = new JsonParser();
    }

    public void showMainPage(){
        System.out.println(this.mainPage);
    }

    public void printMovie(Movie movie){
        System.out.println("Actori: " + movie.getActors());
        System.out.println("An de aparitie: " + movie.getYearOfRelease());
        System.out.println("Gen: " + movie.getType());
        System.out.println("Premii: " + movie.getAwards());
        System.out.println("Rating: " + movie.getRatings());
        System.out.println("IMDB Rating: " + movie.getImdbRating());
        System.out.println(movie.getImdbLink() + "\n");
    }

    // Ma conectez la baza de date; trimit cererea; salvez raspunsul in format json
    public String sendRequest(char searchKey, String whatToSearch) throws IOException {
        apiRequest.buildRequestString(searchKey, whatToSearch);
        apiConnection.openConnection(apiRequest.getRequestString(), "GET");
        return apiConnection.createJsonContent();
    }

    public void chooseOption() throws IOException {

        // Alegerea optiunii...
        int option = input.nextInt();

        // Reader pentru fisiere
        Scanner myReader;
        String data;

        //Writer pentru scriere in fisier
        FileWriter myWriter;

        // Filmul cautat
        Movie searchedMovie = new Movie();

        // Aici salvam raspunsul la cerere in format json
        String jsonContent = "";

        // Daca am gasit filmul "found" devine 1
        int found = 1;

        //imdb ID pentru adaugarea filmului in fisiere
        String ID = "";

        switch (option){
            case 1:
                // Citesc si afisez tot din fisierul cu filme vizionate
                myReader = new Scanner(this.seenMovies);
                System.out.println("1. Filme/Seriale vizionate:\n");
                while(myReader.hasNextLine()){
                    data = myReader.nextLine();
                    System.out.println(data);
                }
                myReader.close();
                break;
            case 2:
                // Citesc si afisez tot din fisierul cu filme de vizionat
                myReader = new Scanner(this.toSeeMovies);
                System.out.println("1. Filme/Seriale de vizionat:\n");
                while(myReader.hasNextLine()){
                    data = myReader.nextLine();
                    System.out.println(data);
                }
                myReader.close();
                break;
            case 3:
                // Ma conectez la OMDB API si caut filmul cerut,
                // salvez informatiile intr-un obiect de tip Movie si le afisez
                System.out.println("Introduceti titlul: ");
                String title = input.next();
                jsonContent = sendRequest('t', title);
                found = jsonParser.Parse(jsonContent, searchedMovie);
                if(found == 1) {
                    printMovie(searchedMovie);
                }
                break;
            case 4:

                //Caut filmul dupa id-ul IMDB
                System.out.println("Introduceti the IMDB ID: ");
                String imdbId = input.next();
                searchedMovie.setRatings("");
                jsonContent = sendRequest('i', imdbId);
                found = jsonParser.Parse(jsonContent, searchedMovie);
                if(found == 1) {
                    printMovie(searchedMovie);
                }
                break;
            case 5:

                // Caut filme dup aun cuvant cheie
                System.out.println("Introduceti cuvantul cheie: ");
                String keyWord = input.next();
                jsonContent = sendRequest('s', keyWord);
                String content = jsonParser.ParseContentFromKeyword(jsonContent);
                if(!content.equals("")) {
                    System.out.println(content);
                }
                break;
            case 6:

                // Scrie in fisierul de filme care au fost deja vizionate
                System.out.println("Introduceti ID: ");
                ID = input.next();
                searchedMovie.setRatings("");
                jsonContent = sendRequest('i', ID);
                found = jsonParser.Parse(jsonContent, searchedMovie);
                if(found == 1) {
                    myWriter = new FileWriter(this.seenMovies);
                    myWriter.write(searchedMovie.getTitle() + " - id imdb: " + searchedMovie.getImdbID());
                    myWriter.close();
                    System.out.println("Done!");
                }
                break;
            case 7:

                // Scrie in fisierul de filme care urmeaza a fi vizionate
                System.out.println("Introduceti ID: ");
                ID = input.next();
                searchedMovie.setRatings("");
                jsonContent = sendRequest('i', ID);
                found = jsonParser.Parse(jsonContent, searchedMovie);
                if(found == 1) {
                    myWriter = new FileWriter(this.toSeeMovies);
                    myWriter.write(searchedMovie.getTitle() + " - id imdb: " + searchedMovie.getImdbID());
                    myWriter.close();
                    System.out.println("Done!");
                }
                break;
            case 8:

                // Afiseaza rezumatul filmului
                System.out.println("Introduceti ID: ");
                ID = input.next();
                searchedMovie.setRatings("");
                jsonContent = sendRequest('i', ID);
                found = jsonParser.Parse(jsonContent, searchedMovie);
                if(found == 1){
                    System.out.println("\n" + searchedMovie.getPlot());
                }
                break;
        }
    }

    public String getMainPage() {
        return mainPage;
    }
}
