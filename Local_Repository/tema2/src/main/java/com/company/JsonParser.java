package com.company;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class JsonParser {

    private JsonObject object;

    public int Parse(String jsonContent, Movie movie){

        // Parsam continutul si il stocăm în obiectul Json
        object = Json.parse(jsonContent).asObject();
        String response = this.object.getString("Response", "Not Found");
        if(response.equals("False")){
            System.out.println("Movie not Found");
            return 0;
        } else {

            // Obținem valoarea câmpului "Title"
            movie.setTitle(this.object.getString("Title", "Not Found"));

            // Obținem valoarea câmpului "Actors"
            movie.setActors(this.object.getString("Actors", "Not Found"));

            // Obținem valoarea câmpului "YearOfRelease"
            movie.setYearOfRelease(this.object.getString("Year", "Not Found"));

            // Obținem valoarea câmpului "Type"
            movie.setType(this.object.getString("Genre", "Not Found"));

            // Obținem valoarea câmpului "Awards"
            movie.setAwards(this.object.getString("Awards", "Not Found"));

            // Obținem Array-ul de obiecte corespunzatoare câmpului "Ratings"
            JsonArray ratings = object.get("Ratings").asArray();
            for (JsonValue item : ratings) {
                // Obținem și afișăm valoarea câmpului "Source"
                String source = item.asObject().getString("Source", "Not Found");

                // Obținem și afișăm valoarea câmpului "Value"
                String value = item.asObject().getString("Value", "Not Found");
                movie.setRatings(movie.getRatings() + source + ": " + value + "; ");
            }

            // Obținem valoarea câmpului "imdbRating"
            movie.setImdbRating(this.object.getString("imdbRating", "Not Found"));

            // Obținem valoarea câmpului "imdbRating"
            movie.setImdbID(this.object.getString("imdbID", "Not Found"));

            // Obținem valoarea câmpului "Plot"
            movie.setPlot(this.object.getString("Plot", "Not Found"));

            // Obținem valoarea câmpului "imdbLink"
            movie.setImdbLink("https://www.imdb.com/title/" + this.object.getString("imdbID", "Not Found") + '/');
            return 1;
        }
    }

    public String ParseContentFromKeyword(String jsonContent){
        String finalContent = "";
        // Parsam continutul si il stocăm în obiectul Json
        object = Json.parse(jsonContent).asObject();
        String response = object.getString("Response", "Not Found");
        if(response.equals("False")){
            System.out.println("Movie not found\n");
        } else {
            JsonArray content = object.get("Search").asArray();
            for (JsonValue item : content) {
                finalContent += item.asObject().getString("Title", "NotFound") + " - " +
                        item.asObject().getString("Year", "Not Found") + " - " +
                        item.asObject().getString("imdbID", "Not Found") + " - " +
                        item.asObject().getString("Type", "Not Found") + "\n";
            }
        }
        return finalContent;
    }
}
